from node import Node

TAB = '\t'


def to_python(node: Node) -> str:
    t = node.type
    if t == 'src':
        return _process_src(node)
    if t == 'Args':
        return _process_args(node)
    if t == 'Function':
        return _process_function(node)
    if t == 'Variable':
        return _process_variable(node)
    if t == 'FunctionCall':
        return _process_functioncall(node)
    if t == 'FuncSin':
        return _process_funcsin(node)
    if t == 'FuncCos':
        return _process_funccos(node)
    if t == 'FuncTan':
        return _process_functan(node)
    if t == 'FuncExp':
        return _process_funcexp(node)
    if t == 'FuncPrint':
        return _process_funcprint(node)
    if t == 'Number':
        return _process_number(node)
    if t == 'BinaryOp':
        return _process_binaryop(node)
    if t == 'AssignOp':
        return _process_assignop(node)
    if t == 'Return':
        return _process_return(node)


def _process_src(node: Node) -> str:
    src = list()
    src.append('from math import sin, cos, tan, exp')
    src.append('')
    src.append('')

    for func in node.parts:
        src.append(to_python(func))

    src.append('if __name__ == \'__main__\':')
    src.append(TAB + 'main()')
    src.append('')
    return '\n'.join(src)


def _process_args(node: Node) -> str:
    src = list()

    for arg in node.parts:  # arg is always will be Node
        src.append(to_python(arg))

    return ', '.join(src)


def _process_function(node: Node) -> str:
    src = list()
    src.append('def ' + str(node.parts[0]) + '(' + to_python(node.parts[1]) + '):')
    for line in node.parts[2]:
        src.append(TAB + to_python(line))
    return '\n'.join(src)


def _process_variable(node: Node) -> str:
    return node.parts[0]


def _process_functioncall(node: Node) -> str:
    return str(node.parts[0]) + '(' + to_python(node.parts[1]) + ')'


def _process_funcsin(node: Node) -> str:
    return 'sin(' + to_python(node.parts[0]) + ')'


def _process_funccos(node: Node) -> str:
    return 'cos(' + to_python(node.parts[0]) + ')'


def _process_functan(node: Node) -> str:
    return 'tan(' + to_python(node.parts[0]) + ')'


def _process_funcexp(node: Node) -> str:
    return 'exp(' + to_python(node.parts[0]) + ')'


def _process_funcprint(node: Node) -> str:
    return 'print(' + to_python(node.parts[0]) + ')'


def _process_number(node: Node) -> str:
    return str(node.parts[0])


def _process_binaryop(node: Node) -> str:
    left = to_python(node.parts[0])
    op = node.parts[1]
    right = to_python(node.parts[2])
    return left + ' ' + op + ' ' + right


def _process_assignop(node: Node) -> str:
    return to_python(node.parts[0]) + ' = ' + to_python(node.parts[1])


def _process_return(node: Node) -> str:
    if len(node.parts) == 1:
        return 'return ' + to_python(node.parts[0]) + '\n\n'
    return 'return' + '\n\n'
