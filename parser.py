from ply.yacc import yacc
from node import Node

from lexer import tokens


def p_src_wrapper(p):
    """src_wrapper : src"""
    p[0] = Node('src', p[1])


def p_src(p):
    """src :
           | src func_decl"""
    if len(p) == 1:
        p[0] = []
    else:
        p[0] = p[1] + [p[2]]


def p_func_decl(p):
    """func_decl : FUNC ID L_ROUND_BRACKET func_args R_ROUND_BRACKET L_CURLY_BRACKET func_body R_CURLY_BRACKET"""
    args = Node('Args', p[4])
    p[0] = Node('Function', [p[2], args, p[7]])


def p_func_body(p):
    """func_body : statement_list statement_return"""
    p[0] = p[1] + [p[2]]


def p_statement_list(p):
    """statement_list :
                      | statement
                      | statement_list statement"""
    if len(p) == 1:
        p[0] = []
    elif len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = p[1] + [p[2]]


def p_statement(p):
    """statement : expr_func_only END_OF_STATEMENT
                 | expr_assign END_OF_STATEMENT"""
    p[0] = p[1]


def p_statement_return(p):
    """statement_return : expr_return END_OF_STATEMENT"""
    p[0] = p[1]


def p_var(p):
    """var : ID"""
    p[0] = Node('Variable', [p[1]])


def p_func(p):
    """func : ID L_ROUND_BRACKET func_args R_ROUND_BRACKET"""
    args = Node('Args', p[3])
    p[0] = Node('FunctionCall', [p[1], args])


def p_func_sin(p):
    """func : SIN L_ROUND_BRACKET expr R_ROUND_BRACKET"""
    p[0] = Node('FuncSin', [p[3]])


def p_func_cos(p):
    """func : COS L_ROUND_BRACKET expr R_ROUND_BRACKET"""
    p[0] = Node('FuncCos', [p[3]])


def p_func_tan(p):
    """func : TAN L_ROUND_BRACKET expr R_ROUND_BRACKET"""
    p[0] = Node('FuncTan', [p[3]])


def p_func_exp(p):
    """func : EXP L_ROUND_BRACKET expr R_ROUND_BRACKET"""
    p[0] = Node('FuncExp', [p[3]])


def p_func_print(p):
    """func : PRINT L_ROUND_BRACKET expr R_ROUND_BRACKET"""
    p[0] = Node('FuncPrint', [p[3]])


def p_func_args(p):
    """func_args :
                 | expr
                 | expr COMMA func_args"""
    if len(p) == 1:
        p[0] = []
    elif len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = [p[1]] + p[3]


def p_expr_number(p):
    """expr : L_NUMBER """
    p[0] = Node('Number', [p[1]])


def p_expr_var(p):
    """expr : var"""
    p[0] = p[1]


def p_expr_func(p):
    """expr : func"""
    p[0] = p[1]


def p_expr_func_only(p):
    """expr_func_only : func"""
    p[0] = p[1]


def p_expr_binary(p):
    """expr : expr OP_ADD expr
            | expr OP_DIFF expr
            | expr OP_MULT expr
            | expr OP_DIV expr"""
    p[0] = Node('BinaryOp', [p[1], p[2], p[3]])


def p_expr_assign(p):
    """expr_assign : var OP_ASSIGN expr"""
    p[0] = Node('AssignOp', [p[1], p[3]])


def p_expr_return(p):
    """expr_return : RETURN
                   | RETURN expr"""
    if len(p) == 2:
        p[0] = Node('Return', [])
    else:
        p[0] = Node('Return', [p[2]])


def p_error(p):
    print("Syntax error in input!", p)


parser = yacc()
