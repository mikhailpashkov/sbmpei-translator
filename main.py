from data import data

from parser import parser
from asm import to_python


tree = parser.parse(data[0])
print('\n=== TREE ===\n')
print(tree)

print('\n=== SRC ===\n')
p_src = to_python(tree)
print(p_src)

print('\n=== RUN ===\n')
with open('./out.py', 'w') as f:
    f.write(p_src)

exec(p_src)
