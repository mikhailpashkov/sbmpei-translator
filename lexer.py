import ply.lex as lex
import re

reserved = {
    'func': 'FUNC',
    'ret': 'RETURN',
    'sin': 'SIN',
    'cos': 'COS',
    'tan': 'TAN',
    'exp': 'EXP',
    'print': 'PRINT',
}

tokens = [
    'ID',
    'END_OF_STATEMENT',
    'L_ROUND_BRACKET',
    'R_ROUND_BRACKET',
    'L_CURLY_BRACKET',
    'R_CURLY_BRACKET',
    'COMMA',

    'OP_ADD',
    'OP_DIFF',
    'OP_MULT',
    'OP_DIV',
    'OP_ASSIGN',

    'L_NUMBER',
] + list(reserved.values())


def t_ID(t):
    r'[a-z][a-zA-Z]*'
    t.type = reserved.get(t.value, 'ID')
    return t


t_END_OF_STATEMENT = r';'
t_L_ROUND_BRACKET = r'\('
t_R_ROUND_BRACKET = r'\)'
t_L_CURLY_BRACKET = r'{'
t_R_CURLY_BRACKET = r'}'
t_COMMA = r'\,'

t_OP_ADD = r'\+'
t_OP_DIFF = r'-'
t_OP_MULT = r'\*'
t_OP_DIV = r'/'

t_OP_ASSIGN = r'='

t_L_NUMBER = r'\d+'

t_ignore = ' '


def t_newline(t):
    r'\n'
    t.lexer.lineno += 1


def t_comment(t):
    r'//.*?(?=\n)'
    pass


def t_error(t):
    print(f'Illegal: {t.value[0]}')


lexer = lex.lex(reflags=re.UNICODE | re.DOTALL)
