data = [
    """
    func test(a, b) {
        ret a / b;
    }
    
    func main() {
        a = sin(test(3, 4));
        b = a * 2;
        print(b / 3);
        ret;
    }
    """,
    """
    func main() {
        a = 1;
        b = 3;
        print(sin(a + b));
        ret;
    }
    """,
]
