class Node:
    def __init__(self, node_type, node_parts):
        self.type = node_type
        self.parts = node_parts

    def add_parts(self, parts):
        self.parts += parts
        return self.parts

    def parts_str(self):
        st = []
        for part in self.parts:
            if type(part) is list:
                st.append('-> [')
                st.append('\n'.join(['\t' + str(p) for p in part]))
                st.append(']')
            else:
                st.append('-> ' + str(part))
        return '\n'.join(st)

    def __repr__(self):
        return self.type + ':\n\t' + self.parts_str().replace('\n', '\n\t')
